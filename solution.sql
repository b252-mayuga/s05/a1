-- 1.
SELECT customerName FROM customers WHERE country = "Philippines";

-- 2.
SELECT contactLastName, contactFirstName FROM customers WHERE customerName = "La Rochelle Gifts";

-- 3.
SELECT productName, MSRP FROM products WHERE productName = "The Titanic";

-- 4.
SELECT firstName, lastName FROM employees WHERE email = "jfirrelli@classicmodelcars.com";

-- 5. 
SELECT customerName FROM customers WHERE state IS NULL;

-- 6.
SELECT firstName, lastName, email FROM employees WHERE lastName = "Patterson" AND firstName = "Steve";

-- 7.
SELECT customerName, country, creditLimit FROM customers WHERE country != "USA" AND creditLimit > 3000;

-- 8.
SELECT customerNumber FROM orders WHERE comments = "DHL";

-- 9.
SELECT productLine FROM productlines WHERE textDescription LIKE "%state of the art%";

-- 10.
SELECT DISTINCT country FROM customers;

-- 11.
SELECT DISTINCT status FROM orders;

-- 12.
SELECT customerName, country FROM customers WHERE country IN ('USA', 'France', 'Canada');

SELECT customerName, country FROM customers WHERE country = 'USA' OR 'France' OR 'Canada';

-- 13.
SELECT firstName, lastName, city FROM employees
	JOIN offices ON employees.officeCode = offices.officeCode WHERE city = "Tokyo";

-- 14.
SELECT customerName FROM customers
	JOIN employees ON customers.salesRepEmployeeNumber = employees.employeeNumber WHERE lastName = "Thompson" AND firstName = "Leslie";

-- 15.
SELECT productName, customerName
FROM products
JOIN orderdetails ON products.productCode = orderdetails.productCode
JOIN orders ON orderdetails.orderNumber = orders.orderNumber
JOIN customers ON orders.customerNumber = customers.customerNumber
WHERE customerName = 'Baane Mini Imports';

-- 16. 
SELECT employees.firstName, employees.lastName, customers.customerName, offices.country
FROM employees
JOIN customers ON employees.employeeNumber = customers.salesRepEmployeeNumber
JOIN offices ON employees.officeCode = offices.officeCode
WHERE customers.country = offices.country
GROUP BY employees.firstName, employees.lastName, customers.customerName, offices.country
ORDER BY employees.lastName, employees.firstName;

-- 17.
SELECT productName, quantityInStock FROM products WHERE productLine = "planes" AND quantityInStock < 1000

-- 18.
SELECT customerName FROM customers WHERE phone LIKE "%+81%";